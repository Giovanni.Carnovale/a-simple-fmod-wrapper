# A Simple FMOD Library
This project is a library design to wrap some of the basic functionalities of FMOD. It was made as the final project for the Sound Programming Course of a Game Dev Course. It supports basic funcionalities in a simple interface that should serve most use-cases.
The Visual Studio Project is divided into the library itself, and an already linked Console Demo, compatible with Windows, to see the library in action.

## Description
The library is contained into the _CarnovaleFMODLibrary_ subproject and it's composed of a single class **_Studio_**, you only need to instantiate it once at the beginning of your application and call its function to use FMOD's functionalities.
The library takes care of memory handling, avoiding leaking, reusing resources.

You can work with channels, use looping, panning, setting volume, pausing and stopping.
Audio supports streaming, loading, in both 3D and 2D.

For most of these use cases the user can call both a convenience method (es. Calling `PlaySound2DStreaming()`) or use a base method and parametrize it as defined by FMOD 
```cpp
PlaySound(const char* Location, int ChannelNumber, FMOD_MODE mode, float StartingVolume, float StartingPan, bool Looping)
```

For fast and convenient management of resources, the library uses a `ChannelData` struct that holds info about channels, the sound they are playing, and some other parameters.

## The Demo
The demo comes into a simple console application using the library. It comes with a couple of Example Sounds one can use to test it, but you are free to add your own and load them using either the Macros already defined.

To make the demo work better with FMOD's asynchronous nature the controls use some Windows only functionalities.

## Installation
Everything is ready to use. You just need to build the library and link it to the project you want.
To try the demo be sure to set it as startup project.
