#include "pch.h"
#include <iostream>
#include "fmod.hpp"
#include "fmod_errors.h"

#include "Studio.h"

using Studio = Core::Studio;


#pragma region Constructors

Core::Studio::Studio() : Core::Studio::Studio(1) {}

Core::Studio::Studio(int MaxChannels, FMOD_INITFLAGS InitFlags /*= FMOD_INIT_NORMAL*/, void* ExtraData /*= 0*/)
	: m_iNumChannels(MaxChannels)
{
	FMOD_RESULT result;
	m_pSystem= nullptr;

	result = FMOD::System_Create(&m_pSystem);
	if (result != FMOD_OK)
	{
		std::cerr<<"FMOD error during System creation; code: "<< result <<" "<<FMOD_ErrorString(result)<<std::endl;
		return;
	}

	result = m_pSystem->init(MaxChannels, InitFlags, ExtraData);
	if (result != FMOD_OK)
	{
		std::cerr << "FMOD error during System Init; code: " << result << " " << FMOD_ErrorString(result) << std::endl;
		return;
	}

	FMOD_VECTOR listener_pos = {0,0,0}, listener_vel = {0,0,0}, listener_forward{0,0,1}, listener_up = {0,1,0};
	m_pSystem->set3DListenerAttributes(0, &listener_pos, &listener_vel, &listener_forward, &listener_up);

	std::cout<<"FMOD Engine is ready"<<std::endl;
}

Core::Studio::~Studio()
{
	for (ChannelContainer::iterator It = m_Channels.begin(); It != m_Channels.end(); ) {
		FMOD_RESULT result;
		result = It->second.m_pPlayingSound->release();
		if (result != FMOD_OK) {
			std::cerr << "Error during sound release on channel " << It->first << "; code " << result << ": " << FMOD_ErrorString(result) << std::endl;
			continue;
		}
		It = m_Channels.erase(It);
	}

	if (m_pSystem) {
		m_pSystem->release();
		m_pSystem = nullptr;
	}
}



Core::Studio::Studio(Studio&& other) noexcept : m_iNumChannels(other.m_iNumChannels) {
	m_pSystem = other.m_pSystem;
	other.m_pSystem = nullptr;
	m_Channels = std::move(other.m_Channels);
}



Studio& Core::Studio::operator=(Studio&& other) noexcept{
	if (this != &other) {
		m_pSystem->release();
		m_pSystem = other.m_pSystem;
		m_iNumChannels = other.m_iNumChannels;
		m_Channels = std::move(other.m_Channels);
		other.m_pSystem = nullptr;
	}

	return *this;
};

#pragma endregion Constructors

void Core::Studio::PlaySound2D(const char* Location, int ChannelNumber, float StartingVolume, float startingPan, bool Looping)
{
	PlaySound(Location, ChannelNumber, FMOD_DEFAULT, StartingVolume ,startingPan, Looping);
}

void Core::Studio::PlaySound3D(const char* Location, int ChannelNumber, float StartingVolume, float startingPan, bool Looping)
{
	PlaySound(Location, ChannelNumber, FMOD_3D, StartingVolume, startingPan, Looping);
}

void Core::Studio::PlaySound2DStreaming(const char* Location, int ChannelNumber, float StartingVolume, float startingPan, bool Looping)
{
	PlaySound(Location, ChannelNumber, FMOD_CREATESTREAM, StartingVolume, startingPan, Looping);
}

void Core::Studio::PlaySound3DStreaming(const char* Location, int ChannelNumber, float StartingVolume, float startingPan, bool Looping)
{
	PlaySound(Location, ChannelNumber, FMOD_CREATESTREAM | FMOD_3D, StartingVolume, startingPan, Looping);
}

void Core::Studio::Stop(int ChannelNumber)
{
	if (ChannelIsPlaying(ChannelNumber)) {
		m_Channels[ChannelNumber].m_pChannel->stop();
		m_Channels[ChannelNumber].m_pPlayingSound->release();
	}
}

void Core::Studio::Tick()
{

	//Garbage Collection
	for (ChannelContainer::iterator It = m_Channels.begin(); It != m_Channels.end();) {
		bool isPlaying;
		FMOD_RESULT result;

		result = It->second.m_pChannel->isPlaying(&isPlaying);
		if (result != FMOD_OK && result != FMOD_ERR_INVALID_HANDLE)
		{
			std::cerr<<"Couldn't retrieve channel "<< It->first<<" state; code "<<result<<": "<<FMOD_ErrorString(result)<<std::endl;
			continue;
		}
		
		bool ReadyForDeletion = !isPlaying || result == FMOD_ERR_INVALID_HANDLE;
		
		if (ReadyForDeletion) {

			if (result != FMOD_OK && result != FMOD_ERR_INVALID_HANDLE) {
				std::cerr<<"Error during sound release on channel "<< It->first<<"; code "<<result<<": "<<FMOD_ErrorString(result)<<std::endl;
				continue;
			}
			It->second.m_pPlayingSound->release();
			It = m_Channels.erase(It);
		}
		else {
			It++;
		}
	}

	m_pSystem->update();	
}

void Core::Studio::SetPan(int NumberChannel, float Pan)
{
	if (ChannelExists(NumberChannel) ) {
		FMOD_MODE mode;
		m_Channels[NumberChannel].m_pPlayingSound->getMode(&mode);
		if (mode & FMOD_3D) {
			_InternalSetPan(NumberChannel, Pan);
		}
		else {
			m_Channels[NumberChannel].m_pChannel->setPan(Pan);
		}
		m_Channels[NumberChannel].m_fPan = Pan;
	}
	else {
		std::cerr<<"Studio::SetPan: Tried to pan invalid channel"<<std::endl;
	}
}

void Core::Studio::PlaySound(const char* Location, int ChannelNumber, FMOD_MODE mode, float StartingVolume, float StartingPan, bool Looping)
{
	if (ChannelNumber < 0 || ChannelNumber > m_iNumChannels) {
		std::cerr << "Studio::PlaySoundInternal: Tried to play in invalid channel"<<std::endl;
		return;
	}
	if (ChannelIsPlaying(ChannelNumber)) {
		m_Channels[ChannelNumber].m_pPlayingSound->release();
	}

	ChannelData DataCache;

	FMOD::Sound* Sound;
	FMOD_RESULT result;

	mode = Looping ? mode | FMOD_LOOP_NORMAL : mode;

	result = m_pSystem->createSound(Location, mode, 0, &Sound);
	if (result != FMOD_OK) {
		std::cerr << "Studio::PlaySoundInternal: Can't create sound object; code: " << result << " " << FMOD_ErrorString(result) << std::endl;
		return;
	}
	DataCache.m_pPlayingSound = Sound;

	FMOD_OPENSTATE State;
	Sound->getOpenState(&State, 0, 0,0);
	while(State == FMOD_OPENSTATE_LOADING) { Sound->getOpenState(&State, 0, 0, 0); }

	//Retrieve requested channel to play sound
	FMOD::Channel* channel;
	result = m_pSystem->getChannel(ChannelNumber, &channel);
	result = m_pSystem->playSound(Sound, nullptr, true, &channel);

	if (result != FMOD_OK) {
		std::cerr << "Studio::PlaySoundInternal: Error playing sound; code: " << result << " " << FMOD_ErrorString(result) << std::endl;
		Sound->release();
		return;
	}

	DataCache.m_pChannel = channel;
	m_Channels[ChannelNumber] = DataCache;

	SetVolume(ChannelNumber, StartingVolume);
	SetPan(ChannelNumber, StartingPan);

	channel->setPaused(false);
}