#pragma once

#include "fmod.hpp"
#include "fmod_errors.h"
#include <iostream>
#include <map>

namespace Core {
	struct ChannelData {
		FMOD::Channel* m_pChannel;
		FMOD::Sound* m_pPlayingSound;
		float m_fVolume = 1.0f;
		float m_fPan = 0.0f;

		inline ChannelData() : m_pChannel(nullptr), m_pPlayingSound(nullptr) {}

		ChannelData(const ChannelData& other) = default;
		ChannelData& operator=(const ChannelData& other) = default;
	};

	class Studio final{
	public:
		Studio();
		Studio(int MaxChannels, FMOD_INITFLAGS InitFlags = FMOD_INIT_NORMAL, void* ExtraData = 0);
		~Studio();

		Studio(const Studio& other) = delete;
		Studio& operator=(const Studio& other) = delete;
		Studio(Studio&& other) noexcept;
		Studio& operator=(Studio&& other) noexcept;

		void PlaySound2D(const char* Location, int ChannelNumber, float StartingVolume = 1, float startingPan = 0, bool Looping = false);
		void PlaySound3D(const char* Location, int ChannelNumber, float StartingVolume = 1, float startingPan = 0, bool Looping = false);
		void PlaySound2DStreaming(const char* Location, int ChannelNumber, float StartingVolume = 1, float startingPan = 0, bool Looping = false);
		void PlaySound3DStreaming(const char* Location, int ChannelNumber, float StartingVolume = 1, float startingPan = 0, bool Looping = false);
		inline void SetPauseChannel(int ChannelNumber, bool Pause) {
			if (ChannelExists(ChannelNumber))
			{
				m_Channels.at(ChannelNumber).m_pChannel->setPaused(Pause);
			} 
		};
		void Stop(int ChannelNumber);

		void Tick();

		inline bool ChannelExists(int Number) const {
			ChannelContainer::const_iterator ChannelIt = m_Channels.find(Number);
			return ChannelIt != m_Channels.end();
		}

		inline bool ChannelIsPlaying(int Number) const { 
			bool isPlaying = false;

			ChannelContainer::const_iterator ChannelIt = m_Channels.find(Number);

			FMOD::Channel* ptr = (ChannelIt != m_Channels.end() ? ChannelIt->second.m_pChannel : nullptr) ;
			if(ptr) ptr->isPlaying(&isPlaying);

			return isPlaying;
		}


		void SetPan(int NumberChannel, float Pan);
		inline float GetPan(int NumberChannel) const {
			if (ChannelExists(NumberChannel)) {
				FMOD_MODE mode;
				m_Channels.at(NumberChannel).m_pPlayingSound->getMode(&mode);
				if (mode & FMOD_3D) {
					FMOD_VECTOR pos, vel;
					m_Channels.at(NumberChannel).m_pChannel->get3DAttributes(&pos, &vel);
					return pos.x / m_fPanRange;
				}
				else {
					return m_Channels.at(NumberChannel).m_fPan;
				}
			}
			std::cerr<<"Studio::GetPan: Requested Channel "<<NumberChannel<<" doesn't exists"<<std::endl;
			return 0;
		}

		inline void SetVolume(int NumberChannel, float Volume) {
			if (ChannelExists(NumberChannel)) {
				m_Channels[NumberChannel].m_pChannel->setVolume(Volume);
				m_Channels[NumberChannel].m_fVolume = Volume;
			}
		};
		inline float GetVolume(int NumberChannel) {
			float volume = -1.f;
			if (ChannelExists(NumberChannel))
			{
				m_Channels[NumberChannel].m_pChannel->getVolume(&volume);
				return volume;
			}
			std::cerr<<"Studio::GetVolume: Tried to change volume of nonexisting channel"<<std::endl;
			return volume;
		}

	
		void PlaySound(const char* Location, int ChannelNumber, FMOD_MODE mode, float StartingVolume = 1.0f, float StartingPan = 0.0f, bool Looping = false);

	private:

		inline void _InternalSetPan(int NumberChannel, float Pan) {
			Pan = (Pan > 1.0f ? 1 : (Pan < -1.0f ? -1 : Pan));
			FMOD_VECTOR pos{ Pan * m_fPanRange,0,0 }, vel{ 0,0,0 };
			FMOD_RESULT result;
			result = m_Channels[NumberChannel].m_pChannel->set3DAttributes(&pos, &vel);
			if (result != FMOD_OK) {
				std::cerr << "Studio::_InternalSetPan: Error setting pan for channel " << NumberChannel << "; code " << result << ": " << FMOD_ErrorString(result) << std::endl;
				return;
			}
		}

		FMOD::System* m_pSystem;
		int m_iNumChannels;
		float m_fPanRange = 1;

		using ChannelContainer = std::map<int, ChannelData>;
		ChannelContainer m_Channels;
	};
}