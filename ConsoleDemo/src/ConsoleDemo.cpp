// ConsoleDemo.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <windows.h>
#undef PlaySound	//Avoid windows.h override

#include "Core.h"

#define EXAMPLES_PATH "..\\Sounds\\"
#define REQUEST_EXAMPLE(File) EXAMPLES_PATH ##File

namespace {
	bool KeyPressed(int KeyCode) {
		return GetAsyncKeyState(KeyCode) & 0x8000;
	}

	float clamp(float value, float min, float max) {
		return value < min ? min : (value > max ? max : value);
	}

	void PrintInfo() {
		std::string stringToPrint =
		"Controls\n1) Select Channel 1\n2) Select Channel 2\nSpacebar) Pause\nVertical Arrows) Set Channel Volume\nHorizontal Arrows) Set Channel Pan\nC) Play Clap\nM) Play Music\nN) Stream Music\nQ) Quit";
		std::string stringToPrint2 =
		"\nE) Enable/Disable Loop\nS) Stop\nL) Play Clap on Left\nR) Play Clap On Right";
		std::cout<<stringToPrint<< stringToPrint2 <<std::endl;
	}

	constexpr float volumeDelta = 0.05f;
	constexpr float panDelta = 0.1f;
	constexpr int SleepDelta = 200;
}

int main()
{
	Core::Studio MyStudio(512);
	//MyStudio.PlaySound2D(REQUEST_EXAMPLE("Clap.mp3"), 1);
	//MyStudio.PlaySound2D(REQUEST_EXAMPLE("Drumloop.wav"), 2);

	bool toggle = true;
	int selectedChannel = -1;
	bool looping = false;
	bool Quit = false;

	PrintInfo();
	while (!Quit) {
		MyStudio.Tick();

		//All the code below just calls the corresponding function in response to an input
		//======QUIT======
		if (KeyPressed('Q')) {
			Quit = true;
			break;
		}
		//======Loop======
		if (KeyPressed('E')) {
			looping = !looping;
			std::cout<<"Looping "<< (looping ? "enabled" : "disabled")<<std::endl;
			Sleep(SleepDelta);
		}
		//======CHANNEL SELECTION======
		if (KeyPressed('1')) {
			std::cout << "Selected Channel 1" << std::endl;
			selectedChannel = 1;
			Sleep(SleepDelta);
		}
		else if (KeyPressed('2'))
		{
			std::cout << "Selected Channel 2" << std::endl;
			selectedChannel = 2;
			Sleep(SleepDelta);
		}

		//======PAUSE======
		if (KeyPressed(VK_SPACE)) {
			MyStudio.SetPauseChannel(selectedChannel, toggle);
			toggle = !toggle;
			Sleep(SleepDelta);
		}

		//======VOLUME AND PANNING======
		if ((KeyPressed(VK_UP) || KeyPressed(VK_DOWN)) && MyStudio.ChannelExists(selectedChannel)) {
			int sign = KeyPressed(VK_UP) ? 1 : -1;
			float currentVolume = MyStudio.GetVolume(selectedChannel);
			currentVolume += sign * volumeDelta;
			currentVolume = clamp(currentVolume, 0, 1);
			MyStudio.SetVolume(selectedChannel, currentVolume);
			std::cout << "Set Volume to " << currentVolume << std::endl;
			Sleep(SleepDelta/2);
		}
		if ((KeyPressed(VK_LEFT) || KeyPressed(VK_RIGHT)) && MyStudio.ChannelExists(selectedChannel)) {
			int sign = KeyPressed(VK_RIGHT) ? 1 : -1;
			float currentPan = MyStudio.GetPan(selectedChannel);
			currentPan += sign * panDelta;
			currentPan = clamp(currentPan, -1, 1);
			MyStudio.SetPan(selectedChannel, currentPan);
			std::cout << "Set Pan to " << currentPan << std::endl;
			Sleep(SleepDelta/2);
		}

		//======SOUND REQUESTING======
		if (KeyPressed('C')) {
			MyStudio.PlaySound2D(REQUEST_EXAMPLE("Clap.mp3"), selectedChannel, 1, 0, looping);
			Sleep(SleepDelta);
		}

		if (KeyPressed('L') || KeyPressed('R')) {
			float Pan = KeyPressed('L') ? -1.0f : 1.0f;
			MyStudio.PlaySound3D(REQUEST_EXAMPLE("Clap.mp3"), selectedChannel,1.0f, Pan, looping);
			MyStudio.Tick();
			Sleep(SleepDelta);
		}

		if (KeyPressed('M')) {
			MyStudio.PlaySound2D(REQUEST_EXAMPLE("Drumloop.wav"), selectedChannel, 1, 0, looping);
			Sleep(SleepDelta);
		}

		if (KeyPressed('N')) {
			MyStudio.PlaySound2DStreaming(REQUEST_EXAMPLE("Drumloop.wav"), selectedChannel, 1, 0, looping);
			Sleep(SleepDelta);
		}

		//======STOPPING======
		if (KeyPressed('S')) {
			MyStudio.Stop(selectedChannel);
		}
	}
	return 0;
}
